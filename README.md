CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers
 * Data to be sent

INTRODUCTION
------------

The module enable to send request to meDRA. The module can be extended by
plugins to send data to other endpoints via Guzzle client.

More info:

 * For a full description of the module, visit
   [the project page](https://www.drupal.org/project/send_request).

 * To submit bug reports and feature suggestions, or to track changes, visit
   [the issue page](https://www.drupal.org/project/issues/send_request).

REQUIREMENTS
------------

 * [Bibliography & Citation](https://www.drupal.org/project/bibcite)

RECOMMENDED MODULES
-------------------

There are no recommended modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See the
   [docs](https://www.drupal.org/documentation/install/modules-themes/modules-8)
   for further information.

CONFIGURATION
-------------

You need at least one content type with the field 'field_bibcite'. The field
have to reference a bibcite entity with at least the following filled fields:
author, bibcite_date, bibcite_doi, bibcite_issn, bibcite_lang, bibcite_pages,
bibcite_secondary_title, bibcite_volume.

### meDRA

Enter all necessary data such as URL, user and password (also for test
environment) at the path /admin/config/services/send-request and define which
content types should trigger the delivery.

**The access data for the live environment:**

 * URL: https://www.medra.org/servlet/ws/upload
 * User: DEMEDRA_???
 * Password: ???

**The access data for the test environment:**

 * URL: https://www-medra-dev.medra.org/servlet/ws/upload
 * User: DEMEDRA_???
 * Password: DEMEDRA_???


If you are on a test system (the HTTP host is checked for '.docker', 'localhost'
or 'ddev'), the access data for the test environment are automatically loaded.
Otherwise is used the settings for the live environment. So please be careful!
Each saving of an article triggers a request.

TROUBLESHOOTING
---------------

There are not any known issues. See:

 * [Issues](https://www.drupal.org/project/issues/send_request)
 * [Automated testing](https://www.drupal.org/node/3136110/qa)
 * [PAReview](https://pareview.sh/pareview/https-git.drupal.org-project-send_request.git)

FAQ
---

There are no answers or issues.

MAINTAINERS
-----------

Current maintainers:

 * [Antonín Slejška](https://www.drupal.org/u/anton%C3%ADn-slej%C5%A1ka)
 * [Kristina Ebel](https://www.drupal.org/u/kristl78)

DATA TO BE SENT
---------------

### meDRA

An XML is created, which is sent to meDRA as text without saving it as a file:

```
<?xml version="1.0" encoding="utf-8"?>
 <ONIXDOISerialArticleVersionRegistrationMessage
     xmlns="http://www.editeur.org/onix/DOIMetadata/1.0"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://www.editeur.org/onix/DOIMetadata/1.0
     http://www.medra.org/schema/onix/DOIMetadata/1.0/ONIX_DOIMetadata_1.0.xsd"
  >
     <Header>
         <FromCompany>Your company</FromCompany>
         <FromEmail>editors-name@your-domain.org</FromEmail>
         <ToCompany>mEDRA</ToCompany>
         <SentDate>20200808</SentDate>
     </Header>
     <DOISerialArticleVersion>
         <NotificationType>06</NotificationType>
         <DOI>10.1109/5.771073</DOI>
         <DOIWebsiteLink>
             http://www.your-domain.org/the-published-article/
         </DOIWebsiteLink>
         <RegistrantName>your organisation</RegistrantName>
         <SerialPublication>
             <SerialWork>
                 <Title>
                     <TitleType>01</TitleType>
                     <TitleText>Your Journal</TitleText>
                 </Title>
                 <Publisher>
                     <PublishingRole>01</PublishingRole>
                     <PublisherName>Your Company</PublisherName>
                 </Publisher>
                 <CountryOfPublication>DE</CountryOfPublication>
             </SerialWork>
             <SerialVersion>
                 <ProductIdentifier>
                     <ProductIDType>07</ProductIDType>
                     <IDValue>0033221X</IDValue>
                 </ProductIdentifier>
                 <ProductForm>JD</ProductForm>
             </SerialVersion>
         </SerialPublication>
         <JournalIssue>
             <JournalIssueDesignation>
                 The Journal 100, 700-720
             </JournalIssueDesignation>
             <JournalIssueDate>
                 <DateFormat>00</DateFormat>
                 <Date>20200808</Date>
             </JournalIssueDate>
         </JournalIssue>
         <ContentItem>
             <TextItem>
                 <PageRun>
                     <FirstPageNumber>700</FirstPageNumber>
                     <LastPageNumber>720</LastPageNumber>
                 </PageRun>
             </TextItem>
             <Title>
                 <TitleType>01</TitleType>
                 <TitleText>The title of the article</TitleText>
             </Title>
             <Contributor>
                 <ContributorRole>A01</ContributorRole>
                 <PersonName>Name of the author</PersonName>
             </Contributor>
             <Language>
                 <LanguageRole>01</LanguageRole>
                 <LanguageCode>ger</LanguageCode>
             </Language>
             <PublicationDate>20200808</PublicationDate>
         </ContentItem>
     </DOISerialArticleVersion>
 </ONIXDOISerialArticleVersionRegistrationMessage>
```

After sending the request to meDRA, the editor will receive a confirmation
e-mail from meDRA.
