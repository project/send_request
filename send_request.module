<?php

/**
 * @file
 * This module send data to an endpoint via GuzzleClient.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_help().
 *
 * @inheritdoc
 */
function send_request_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.send_request':
      $text = file_get_contents(__DIR__ . '/README.md');
      if (!Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . Html::escape($text) . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = Drupal::service('plugin.manager.filter');
        $settings = Drupal::configFactory()
          ->get('markdown.settings')
          ->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}

/**
 * Implements hook_node_insert().
 *
 * After saving a node in db we have ID and URL etc.
 *
 * @inheritdoc
 */
function send_request_node_insert(EntityInterface $node) {
  _send_request_after_save($node);
}

/**
 * Implements hook_node_update().
 *
 * @inheritdoc
 */
function send_request_node_update(EntityInterface $node) {
  _send_request_after_save($node, TRUE);
}

/**
 * Utility function to send the request after saving the node.
 *
 * @param \Drupal\Core\Entity\EntityInterface $node
 *   Node, which should be used, when preparing the request.
 * @param bool $update
 *   If the function is called from node_update hook.
 *
 * @return bool
 *   Return true, if everything is ok.
 */
function _send_request_after_save(EntityInterface $node, $update = FALSE) {
  // Check if we have enabled modules.
  $config = \Drupal::configFactory()->getEditable('send_request.settings');

  // Test if meDRA is activated.
  $medraActivated = (
    $config->get('medra_activated') == 1 &&
    !empty($config->get('medra_activation_field')) &&
    $node->{$config->get('medra_activation_field')} &&
    $node->{$config->get('medra_activation_field')}[0]->getValue()['value'] == 1
  ) ? TRUE : FALSE;

  // Send request to meDRA.
  if ($medraActivated) {
    // Get settings data for content types.
    $contentTypes = explode('|', $config->get('medra_content_types'));
    // If content type is not enabled => leave this hook.
    if (!in_array($node->bundle(), $contentTypes)) {
      $message = 'The content types are not correctly set.';
      \Drupal::logger('send_request')->notice($message);
      return FALSE;
    }

    // Get data.
    $meDRAPlugin = \Drupal::service('send_request.plugin.meDRA');
    $data = $meDRAPlugin->createData($node, $update);
    if (!$data) {
      $message = 'The meDRA plugin could not create data.';
      \Drupal::logger('send_request')->notice($message);
      return FALSE;
    }

    // Send the data.
    $client = \Drupal::service('send_request.client');
    // If there is an error you can see it in /admin/reports/dblog.
    $response = $client->sendDataToEndpoint($meDRAPlugin, $data, $node);
    // Handle the response.
    if ($response !== FALSE) {
      $meDRAPlugin->handleResponse($response);
      return TRUE;
    }
  }
  return FALSE;
}
