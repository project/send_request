<?php

namespace Drupal\send_request;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a list of Send Request endpoint entities.
 */
class EndpointListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    // Prepare the table header.
    $header = [];
    $header['id'] = $this->t('Id');
    $header['name'] = $this->t('Name of the request');
    $header['type'] = $this->t('Request type');
    $header['url'] = $this->t('URL');
    $header['user'] = $this->t('Username');
    $header['password'] = $this->t('Password');
    $header['test'] = $this->t('Test');
    $header['doi_prefix'] = $this->t('DOI prefix');

    // Return the table header.
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $send_request_endpoint = $entity;

    // Prepare the table row for the endpoint.
    $row = [];
    $row['id'] = $send_request_endpoint->id();
    $row['name'] = $send_request_endpoint->name;
    $row['type'] = $send_request_endpoint->type;
    $row['url'] = $send_request_endpoint->url;
    $row['user'] = $send_request_endpoint->user;
    $row['password'] = $send_request_endpoint->password;
    $row['test'] = $send_request_endpoint->test;
    $row['doi_prefix'] = $send_request_endpoint->doi_prefix;

    // Return the table row.
    return $row + parent::buildRow($send_request_endpoint);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $send_request_endpoint = $entity;

    // Prepare the table row operations for the endpoint.
    $operations = parent::getDefaultOperations($send_request_endpoint);
    if ($send_request_endpoint->hasLinkTemplate('edit')) {
      $operations['edit'] = [
        'title' => $this->t('Edit endpoint'),
        'weight' => 20,
        'url' => $send_request_endpoint->toUrl('edit'),
      ];
    }
    if ($send_request_endpoint->hasLinkTemplate('delete')) {
      $operations['delete'] = [
        'title' => $this->t('Delete endpoint'),
        'weight' => 40,
        'url' => $send_request_endpoint->toUrl('delete'),
      ];
    }

    // Return the table row operations.
    return $operations;
  }

}
