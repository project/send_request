<?php

namespace Drupal\send_request\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Send Request endpoint entity.
 *
 * @ConfigEntityType(
 *   id = "send_request_endpoint",
 *   label = @Translation("Send Request endpoint"),
 *   module = "send_request",
 *   config_prefix = "send_request_endpoint",
 *   handlers = {
 *     "list_builder" = "Drupal\send_request\EndpointListBuilder",
 *     "form" = {
 *       "add" = "Drupal\send_request\Form\EndpointForm",
 *       "edit" = "Drupal\send_request\Form\EndpointForm",
 *       "delete" = "Drupal\send_request\Form\EndpointDeleteForm",
 *     }
 *   },
 *   admin_permission = "administer bibcite",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 * config_export = {
 *     "id",
 *     "name",
 *     "type",
 *     "url",
 *     "user",
 *     "password",
 *     "test",
 *     "doi_prefix"
 *   },
 *   links = {
 *     "edit" = "/admin/config/bibcite/send-request/{send_request_endpoint}",
 *     "delete" = "/admin/config/bibcite/send-request/{send_request_endpoint}/delete",
 *   },
 * )
 */
class EndpointEntity extends ConfigEntityBase implements EndpointEntityInterface {

  /**
   * ID of the Endpoint Entity.
   *
   * @var string
   */
  public $id = NULL;

  /**
   * Name of the request.
   *
   * @var string
   */
  public $name = NULL;

  /**
   * Type of the request (recently nur 'medra').
   *
   * @var string
   */
  public $type = 'medra';

  /**
   * URL to which the request should be sent.
   *
   * @var string
   */
  public $url = NULL;

  /**
   * Username of the request account.
   *
   * @var string
   */
  public $user = NULL;

  /**
   * Password of the request account.
   *
   * @var string
   */
  public $password = NULL;

  /**
   * Test endpoint.
   *
   * If true, then the endpoint can be used also in development mode (for
   * testing).
   *
   * @var bool
   */
  public $test = 0;

  /**
   * DOI prefix.
   *
   * DOI prefix is used by mEDRA to recognise, which account/endpoint should be
   * used for the request. For an article will be used the endpoint, which has
   * the appropriate DOI prefix.
   *
   * @var string
   */
  public $doi_prefix = NULL;

}
