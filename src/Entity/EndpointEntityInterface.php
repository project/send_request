<?php

namespace Drupal\send_request\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for Send Request endpoints.
 */
interface EndpointEntityInterface extends ConfigEntityInterface {
}
