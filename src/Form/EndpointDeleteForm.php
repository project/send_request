<?php

namespace Drupal\send_request\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Endpoint delete form for send_request.
 */
class EndpointDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $send_request_endpoint = $this->entity;
    return $this->t('Are you sure you want to delete the endpoint %url from the control list?', ['%url' => $send_request_endpoint->url]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.send_request_endpoint');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $send_request_endpoint = $this->entity;
    // Delete the endpoint and display the status message.
    try {
      $send_request_endpoint->delete();
      $this->logger('send_request')->info('The endpoint %url was deleted successfully.', ['%url' => $send_request_endpoint->url]);
      \Drupal::messenger()->addStatus($this->t('The endpoint %url was deleted successfully.', ['%url' => $send_request_endpoint->url]));
    }
    catch (EntityStorageException $exception) {
      $this->logger('send_request')->error('The endpoint %url was not deleted.', ['%url' => $send_request_endpoint->url]);
      \Drupal::messenger()->addError($this->t('The endpoint %url was not deleted.', ['%url' => $send_request_endpoint->url]));
    }
    // Set form redirection.
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
