<?php

namespace Drupal\send_request\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Endpoint form for send_request.
 */
class EndpointForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $send_request_endpoint = $this->entity;

    // Prepare the form.
    $form = parent::form($form, $form_state);

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of the request'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $send_request_endpoint->name,
      '#description' => $this->t('The name is used to generate the ID.'),
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#disabled' => !$send_request_endpoint->isNew(),
      '#default_value' => $send_request_endpoint->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => ['name'],
      ],
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of the request'),
      '#required' => TRUE,
      '#default_value' => $send_request_endpoint->type,
      '#options' => ['medra' => 'mEDRA'],
    ];

    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $send_request_endpoint->url,
      '#description' => $this->t('Please enter here the URL to which the request should be sent.'),
    ];

    $form['user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $send_request_endpoint->user,
      '#description' => $this->t('Please enter the username for the request here.'),
    ];

    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $send_request_endpoint->password,
      '#description' => $this->t('Please enter the password for the request here.'),
    ];

    $form['test'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Test'),
      '#default_value' => $send_request_endpoint->test,
      '#description' => $this->t('Please check, if this endpoint is only for test purposes.'),
    ];

    $form['doi_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('DOI prefix'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => FALSE,
      '#default_value' => $send_request_endpoint->doi_prefix,
      '#description' => $this->t('Please enter the DOI prefix for the mEDRA account here. This field should be used only for mEDRA requests!'),
    ];

    // Return the form.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Retrieve $url (which, being required, is surely not blank).
    $url = $form_state->getValue('url');

    // Perform URL validation:
    if (0 < mb_strlen($url)) {
      if (!is_string(filter_var($url, FILTER_VALIDATE_URL))) {
        $form_state->setErrorByName('url', $this->t('%url is not a valid URL.', ['%url' => $url]));
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $send_request_endpoint = $this->entity;
    // Save the endpoint and display the status message.
    try {
      $send_request_endpoint->save();
    }
    catch (EntityStorageException $exception) {
      $this->logger('send_request')->error('The endpoint %url was not saved.', ['%url' => $send_request_endpoint->url]);
      $this->messenger()->addError($this->t('The endpoint %url was not saved.', ['%url' => $send_request_endpoint->url]));
    }
    // Redirect to the proper url.
    $form_state->setRedirect('entity.send_request_endpoint');
  }

  /**
   * Checks if the endpoint exists.
   *
   * @param string $id
   *   Machine name (ID) of the Send Request endpoint.
   *
   * @return bool
   *   Returns true, if the endpoint exist.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists($id) {
    $storage = $this->entityTypeManager->getStorage('send_request_endpoint');
    $send_request_endpoint = $storage->getQuery()->condition('id', $id)->execute();
    return (bool) $send_request_endpoint;
  }

}
