<?php

namespace Drupal\send_request\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\send_request\Plugin\MeDRAPlugin;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configures Send Request settings for this site.
 */
class SendRequestSettingsForm extends ConfigFormBase {

  /**
   * Plugin for the meDRA endpoint.
   *
   * @var \Drupal\send_request\Plugin\MeDRAPlugin
   */
  protected $meDRAPlugin;

  /**
   * Class constructor.
   *
   * @param \Drupal\send_request\Plugin\MeDRAPlugin $medra_plugin
   *   meDRA plugin.
   */
  public function __construct(MeDRAPlugin $medra_plugin) {
    $this->meDRAPlugin = $medra_plugin;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('send_request.plugin.meDRA')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'send_request_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['send_request.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('send_request.settings');

    // General form.
    $form['general'] = [
      '#type' => 'details',
      '#title' => 'General',
      '#open' => TRUE,
    ];
    $form['general']['company'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your company'),
      '#description' => $this->t('Will be used e.g. at meDRA requests: $content[Header][FromCompany].'),
      '#default_value' => $config->get('company'),
      '#required' => TRUE,
    ];
    $form['general']['country_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Country of publication (code)'),
      '#description' => $this->t('Please enter the country code, e.g.: EN, DE or FR.'),
      '#default_value' => $config->get('country_code'),
      '#required' => TRUE,
    ];

    // Build form for meDRA.
    $form = $this->meDRAPlugin->buildForm($form, $config);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate form for meDRA.
    $this->meDRAPlugin->validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('send_request.settings');
    // Create submit for meDRA.
    $config->set('company', trim($form_state->getValue('company')));
    $config->set('country_code', trim($form_state->getValue('country_code')));
    $config = $this->meDRAPlugin->submitForm($config, $form_state);
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
