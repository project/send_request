<?php

namespace Drupal\send_request\Plugin;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\send_request\Entity\EndpointEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Plugin for sending data to meDRA endpoint.
 *
 * @package Drupal\send_request\Plugin
 */
class MeDRAPlugin {
  use StringTranslationTrait;

  /**
   * Develop mode.
   *
   * @var bool
   */
  protected $developMode = FALSE;

  /**
   * Configurations.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack object.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   */
  public function __construct(ConfigFactory $config_factory, LoggerChannelFactoryInterface $logger_factory, TranslationInterface $string_translation, RequestStack $request_stack, AccountInterface $current_user) {
    $this->config = $config_factory->getEditable('send_request.settings');
    $this->logger = $logger_factory->get('send_request');
    $this->stringTranslation = $string_translation;
    $this->currentUser = $current_user;

    $currentHost = $request_stack->getCurrentRequest()->getHttpHost();

    // Test if the site is in develop mode.
    if (strpos($currentHost, '.docker') !== FALSE || strpos($currentHost, 'localhost') !== FALSE || strpos($currentHost, 'ddev') !== FALSE) {
      $this->logger->notice('Plugin <strong>MeDRAPlugin</strong> is in develop mode.');
      $this->developMode = TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('string_translation'),
      $container->get('request_stack'),
      $container->get('current_user')
    );
  }

  /**
   * Get endpoint.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   Node used, when preparing the request.
   *
   * @return \Drupal\send_request\Entity\EndpointEntity|bool
   *   Returns endpoint configuration entity or FALSE.
   */
  public function getEndpoint($node) {
    $endpoint = FALSE;
    $bibciteEntity = $this->getBibciteEntity($node);
    $entityDoi = trim($bibciteEntity->bibcite_doi->value);
    $regex = '/^10.[0-9]{4}\/[A-Z0-9-]{3,25}$/';
    if (is_string($entityDoi) && preg_match($regex, $entityDoi)) {
      // Get the endpoint from endpoint configuration entities.
      $entityDoiPrefix = substr($entityDoi, 0, 7);
      foreach (EndpointEntity::loadMultiple() as $endpointEntity) {
        if ($endpointEntity->type === 'medra' && $endpointEntity->doi_prefix === $entityDoiPrefix) {
          $endpoint = $endpointEntity;
          break;
        }
      }
    } else {
      $this->logger->error('The article has a faulty DOI.');
      return FALSE;
    }
    if ($endpoint && $this->developMode) {
      if ($endpoint->test == 1) {
        $this->logger->notice('Plugin <strong>MeDRAPlugin</strong> use the test URL <strong>' . $endpoint->url . '</strong>');
      } else {
        $this->logger->error('In develop mode only request to test endpoints may be sent.');
        return FALSE;
      }
    }
    return $endpoint;
  }

  /**
   * Get method.
   */
  public function getMethod() {
    return 'POST';
  }

  /*
   * Get bibcite entity.
   */
  private function getBibciteEntity($node) {
    // Check if we have bibsite.
    if (!$node->hasField('field_bibcite')) {
      return FALSE;
    }
    $bibciteEntities = $node->field_bibcite->referencedEntities();
    if (!isset($bibciteEntities[0])) {
      return FALSE;
    }

    // Bibcite data.
    return $bibciteEntities[0];
  }

  /**
   * Create data.
   */
  public function createData($node, $update) {
    $bibciteEntity = $this->getBibciteEntity($node);
    if ($bibciteEntity === FALSE) {
      return;
    }

    // Create Authors XML array.
    $authors = [];
    $sequenceNumber = 0;
    foreach ($bibciteEntity->author->referencedEntities() as $authorRef) {
      $sequenceNumber++;
      $authors[] = [
        'SequenceNumber' => $sequenceNumber,
        'ContributorRole' => 'A01',
        'PersonName' => $authorRef->get('first_name')->value . ' ' . $authorRef->get('last_name')->value,
      ];
    }
    // Current user data.
    $currentUserAccount = $this->currentUser->getAccount();
    // Absolute url.
    $nodeUrl = $node->toUrl();
    $nodeUrl->setAbsolute();
    // JournalIssueDesignation.
    $journalIssueDesignation = trim($bibciteEntity->bibcite_secondary_title->value) . ' ' . trim($bibciteEntity->bibcite_volume->value);
    $firstPageNumber = 0;
    $lastPageNumber = 0;
    if (trim($bibciteEntity->bibcite_pages->value) != '') {
      $journalIssueDesignation .= ', ' . trim($bibciteEntity->bibcite_pages->value);
      // Get pages.
      if (strpos($bibciteEntity->bibcite_pages->value, '-') !== FALSE) {
        $pagesExplode = explode('-', trim($bibciteEntity->bibcite_pages->value));
        $firstPageNumber = (int) trim($pagesExplode[0]);
        $lastPageNumber = (int) trim($pagesExplode[1]);
      }
    }

    // Publication date.
    $dateArray = explode('/', trim($bibciteEntity->bibcite_date->value));
    $publicationDate = new \DateTime();
    $publicationDate->setDate($dateArray[1], $dateArray[0], 1);

    // Define product form code. Default value is JD.
    $productFormCode = 'JD';
    $formFieldName = $this->config->get('medra_product_form_field');
    $formFieldSettings = $this->config->get('medra_product_form_field_values');
    if ($formFieldName && $formFieldSettings && $formField = $node->{$formFieldName}) {
      $formFieldValues = $formField->getValue();
      $settingsArray = explode('|', $formFieldSettings);
      foreach ($formFieldValues as $formFieldValue) {
        foreach ($settingsArray as $settings) {
          $settings = explode(':', $settings);
          if ($formFieldValue['target_id'] == $settings[0]) {
            $productFormCode = $settings[1];
            break;
          }
        }
      }
    }

    // Get Language.
    $publishLang = 'eng';
    $bibciteLangValue = trim($bibciteEntity->bibcite_lang->value);
    if ($bibciteLangValue !== 'English' && $bibciteLangValue !== '') {
      // The first three lowercased letters.
      $publishLang = strtolower(substr($bibciteLangValue, 0, 3));
    }
    // Create data array.
    $content = [
      'Header' => [
        /*'NotificationResponse' => '02',*/
        'FromCompany' => $this->config->get('company'),
        'FromEmail' => $currentUserAccount->getEmail(),
        'ToCompany' => 'mEDRA',
        'SentDate' => date('Ymd'),
      ],
      'DOISerialArticleVersion' => [
        'NotificationType' => ($update === FALSE || $this->developMode === TRUE) ? '07' : '06',
        'DOI' => trim($bibciteEntity->bibcite_doi->value),
        'DOIWebsiteLink' => $nodeUrl->toString(),
        'RegistrantName' => $this->config->get('medra_registrant_name'),
        'SerialPublication' => [
          'SerialWork' => [
            'Title' => [
              'TitleType' => '01',
              'TitleText' => trim($bibciteEntity->bibcite_secondary_title->value),
            ],
            'Publisher' => [
              'PublishingRole' => '01',
              'PublisherName' => trim($bibciteEntity->bibcite_publisher->value),
            ],
            'CountryOfPublication' => $this->config->get('country_code'),
          ],
          'SerialVersion' => [
            'ProductIdentifier' => [
              'ProductIDType' => '07',
              'IDValue' => trim($bibciteEntity->bibcite_issn->value),
            ],
            'ProductForm' => $productFormCode,
          ],
        ],
        'JournalIssue' => [
          'JournalIssueDesignation' => $journalIssueDesignation,
          'JournalIssueDate' => [
            'DateFormat' => '00',
            'Date' => $publicationDate->format('Ymd'),
          ],
        ],
        'ContentItem' => [
          'TextItem' => [
            'PageRun' => [
              'FirstPageNumber' => $firstPageNumber,
              'LastPageNumber' => $lastPageNumber,
            ],
          ],
          'Title' => [
            'TitleType' => '01',
            'TitleText' => trim($node->title->value),
          ],
          'CompositeContributor' => $authors,
          'Language' => [
            'LanguageRole' => '01',
            'LanguageCode' => $publishLang,
          ],
          'PublicationDate' => $publicationDate->format('Ymd'),
        ],
      ],
    ];

    // Creating object of SimpleXMLElement.
    $xmlStartElement = '<?xml version="1.0" encoding="UTF-8"?> <ONIXDOISerialArticleVersionRegistrationMessage xmlns="http://www.editeur.org/onix/DOIMetadata/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.editeur.org/onix/DOIMetadata/2.0 https://www.medra.org/schema/onix/DOIMetadata/2.0/ONIX_DOIMetadata_2.0.xsd"></ONIXDOISerialArticleVersionRegistrationMessage>';
    $xmlData = new \SimpleXMLElement($xmlStartElement);
    $this->arrayToXml($content, $xmlData);
    $xmlAsString = $xmlData->asXML();

    // Saving generated xml file.
    $fileName = 'upload' . $node->get('nid')->getValue()[0]['value'];

    $data = [
      'file' => [
        'name' => $fileName,
        'content' => $xmlAsString,
        'extension' => 'xml',
      ],
      'LANG' => strtolower($this->config->get('country_code')),
    ];

    return $data;
  }

  /**
   * Handle the response.
   *
   * @todo react on the response from meDRA, see:
   * @link https://www-medra-dev.medra.org/stdoc/mEDRA_WebService_HTTP_callback_User_Guidev1.0.pdf
   */
  public function handleResponse($response) {
    return $response;
  }

  /**
   * Build the form.
   */
  public function buildForm($form, $config) {
    $form['medra'] = [
      '#type' => 'details',
      '#title' => 'meDRA data transmission',
      '#open' => TRUE,
    ];
    $form['medra']['medra_activated'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Data transmission activated'),
      '#description' => $this->t('After creating a node, an XML file is sent to https://www.medra.org/servlet/ws/upload.'),
      '#default_value' => $config->get('medra_activated'),
    ];
    $form['medra']['medra_registrant_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Registrant name'),
      '#description' => $this->t('Please enter the registrant name for the meDRA service.'),
      '#default_value' => $config->get('medra_registrant_name'),
    ];

    $form['medra']['medra_content_types'] = [
      '#type' => 'textfield',
      '#title' => $this->t('For which content type(s)'),
      '#description' => $this->t('The data of which content types should be sent? Please separate with |.'),
      '#default_value' => $config->get('medra_content_types'),
    ];
    $form['medra']['medra_activation_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Activation field'),
      '#description' => $this->t('Name of the activation field, e.g.: field_send_request. (The field must be a checkbox.) If set, then the request will be sent only, if the field is checked. If empty, then the request will be sent by every change of the node.'),
      '#default_value' => $config->get('medra_activation_field'),
    ];

    $form['medra']['medra_product_form_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product form field'),
      '#description' => $this->t('The default product form code is JD. You can set a field (e.g. a taxonomy field), which will be used to redefine the product form.'),
      '#default_value' => $config->get('medra_product_form_field'),
    ];
    $form['medra']['medra_product_form_field_values'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product form field values'),
      '#description' => $this->t('The values are separated with |. Example: 79:JB|80:JC (If the term id is 79, then the code JB will be used, if the term id is 80, then the code JC will be used.'),
      '#default_value' => $config->get('medra_product_form_field_values'),
    ];

    return $form;
  }

  /**
   * Validate the form.
   */
  public function validateForm(&$form, &$form_state) {
    // Build form for meDRA.
    if ($form_state->getValue('medra_activated') == 1) {
      // Check registrant name.
      if (trim($form_state->getValue('medra_registrant_name')) == '') {
        $form_state->setErrorByName('medra_registrant_name',
          $this->t('Registrant name must be set.')
        );
      }
      // Check content type.
      if (trim($form_state->getValue('medra_content_types')) == '') {
        $form_state->setErrorByName('medra_content_types',
          $this->t('"For which content type(s)" must be set.')
        );
      }
    }
  }

  /**
   * Submit the form.
   */
  public function submitForm($config, $form_state) {
    $config
      ->set('medra_activated', $form_state->getValue('medra_activated'))
      ->set('medra_registrant_name', trim($form_state->getValue('medra_registrant_name')))
      ->set('medra_content_types', trim($form_state->getValue('medra_content_types')))
      ->set('medra_activation_field', trim($form_state->getValue('medra_activation_field')))
      ->set('medra_product_form_field', trim($form_state->getValue('medra_product_form_field')))
      ->set('medra_product_form_field_values', trim($form_state->getValue('medra_product_form_field_values')));

    return $config;
  }

  /**
   * Transform an array to XML.
   *
   * If an array key starts with 'Composite', then this element have to contain
   * an array. All item of the array will be transformed to identical XML
   * elements, e.g.:
   *    <Contributor>
   *      <ContributorRole>A01</ContributorRole>
   *      <PersonName> L Falgenhauer </PersonName>
   *    </Contributor>
   *    <Contributor>
   *      <ContributorRole>A01</ContributorRole>
   *      <PersonName> J Schmiedel </PersonName>
   *    </Contributor>
   */
  private function arrayToXml($data, &$xmlData) {
    foreach ($data as $key => $value) {
      if (is_numeric($key)) {
        // Dealing with <0/>..<n/> issues.
        $key = 'item' . $key;
      }
      if (is_array($value)) {
        // Test if it is a composite element.
        if (substr( $key, 0, 9 ) === "Composite") {
          $compositeKey = str_replace('Composite', '', $key);
          foreach ($value as $item) {
            $subnode = $xmlData->addChild($compositeKey);
            $this->arrayToXml($item, $subnode);
          }
        } else {
          $subnode = $xmlData->addChild($key);
          $this->arrayToXml($value, $subnode);
        }
      } else {
        $xmlData->addChild("$key", htmlspecialchars("$value"));
      }
    }
  }

}
