<?php

namespace Drupal\send_request;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Client sending the requests to the endpoints.
 */
class SendRequestClient {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Construct a Schema client object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   */
  public function __construct(ConfigFactory $config_factory, ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory) {
    $this->config = $config_factory->getEditable('send_request.settings');
    $this->httpClient = $http_client;
    $this->logger = $logger_factory->get('send_request');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('logger.factory')
    );
  }

  /**
   * Send data to endpoint.
   *
   * @param mixed $endpointPlugin
   *   The plugin used to send the data to the endpoint.
   * @param array $data
   *   Data to send.
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   Node used, when preparing the request.
   *
   * @return mixed
   *   Returns response or false, if the response there is no valid response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function sendDataToEndpoint($endpointPlugin, array $data, $node) {
    // Get endpoint.
    $endpoint = $endpointPlugin->getEndpoint($node);
    // Get method.
    $method = $endpointPlugin->getMethod();

    // Set data to "form".
    if (count($data) > 0 && $endpoint) {
      $options = [
        'http_errors' => FALSE,
        'headers' => [
          'Content-Language' => strtolower($this->config->get('country_code')),
          'Content-Type' => 'application/xml'
        ],
        'auth' => [
          $endpoint->user,
          $endpoint->password,
        ],
        'body' => ''
      ];
      $dataForLogging = '';

      foreach ($data as $name => $content) {
        if ($name == 'file') {
          $options['body'] = $data['file']['content'];
          $dataForLogging .= $name . ' ' . $data['file']['name'] . ': <em>' . htmlentities($data['file']['content']) . '</em><br />';
          break;
        }
        else {
          $options['body'] = $content;
          $dataForLogging .= $name . ': ' . $content . ';<br />';
        }
        $dataForLogging .= '<br />';
      }

      try {
        $response = $this->httpClient->request($method, $endpoint->url, $options);
        $code = $response->getStatusCode();
        if ($code == 200) {
          // Logging.
          $this->logger->notice('Plugin <strong>' . get_class($endpointPlugin) . '</strong> has sent the following data to <strong>' . $endpoint->url . '</strong>:<br />' . $dataForLogging . '<br />Body: ' . $response->getBody());
        }
        else {
          // Logging.
          $this->logger->error('Plugin <strong>' . get_class($endpointPlugin) . '</strong> has sent the following data to <strong>' . $endpoint->url . '</strong>.<br />Status code: ' . $code . '<br />Data: ' . $dataForLogging . '<br />Body: ' . $response->getBody());
        }
        return $response;
      }
      catch (RequestException $e) {
        \Drupal::logger('send_request')->error('Job has been rejected with following error: @error', ['@error' => $e->getMessage()]);
        return FALSE;
      }
    }
  }

}
